﻿import java.util.*;


public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        String str = "Зима Снег Стол Ёлка Java Java Снег Снег";
        String[] words = str.split(" ");

        for (int i=0; i < words.length; i++) {
            Integer count = map.get(words[i]) ;
            map.put(words[i],count==null ? 1 : count+1);
        }
	

	//версия 3
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Слово - "+entry.getKey() + " встречается "+ entry.getValue() + " раз.");
        }
   }
}

